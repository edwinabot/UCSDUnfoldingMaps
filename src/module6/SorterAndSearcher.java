package module6;

import java.util.Random;

/**
 * Created by edwin on 28/10/16.
 */
public class SorterAndSearcher {

    /**
     * Creates an array of random integers
     *
     * @param arrayLength     amount of elements to be randomized
     * @param maxIntegerValue maximum value that any element of the array may represent
     * @return an integer array
     */
    public Integer[] generateIntegerArray(Integer arrayLength, Integer maxIntegerValue) {
        Integer[] array = new Integer[arrayLength];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(maxIntegerValue);
        }
        return array;
    }


    public Comparable linearSearch(Comparable[] array, Comparable toFind) {
        int i = 0;
        while (i < array.length) {
            if (array[i].equals(toFind)) {
                return array[i];
            }
            i++;
        }
        return null;
    }

    public Comparable binarySearch(Comparable[] array, Comparable toFind) {
        Integer min = 0;
        Integer max = array.length;
        Integer med;
        while (min <= max) {
            med = min + ((max - min) / 2);
            Integer vs = toFind.compareTo(array[med]);
            if (vs < 0) {
                max = med - 1;
            } else if (vs == 0) {
                return array[med];
            } else {
                min = med + 1;
            }
        }
        return null;
    }


    /**
     * Swap two elements in an array
     *
     * @param array the containing array
     * @param pos_a first element to swap
     * @param pos_b second element to swap
     */
    private void swapElements(Comparable[] array, Integer pos_a, Integer pos_b) {
        if (pos_a.equals(pos_b)) {
            return;
        }
        Comparable aux = array[pos_a];
        array[pos_a] = array[pos_b];
        array[pos_b] = aux;
    }

    public void selectionSort(Comparable[] array) {
        /*
        For each index ('i') of the array
            I assume that the element at 'i' is the smallest
            For each element starting from the next index to 'i' ('j')
                If the element at 'j' is smaller than the current smallest
                    Now the smallest becomes 'j'
            swap 'i' and the smallest
        end of the algorithm
         */

        for (Integer i = 0; i < array.length - 1; i++) {
            Integer smallest_pos = i;
            for (Integer j = i + 1; j < array.length; j++) {
                if (array[j].compareTo(array[smallest_pos]) < 0) {
                    smallest_pos = j;
                }
            }

            swapElements(array, smallest_pos, i);
        }
    }

    public void insertionSort(Comparable[] array) {
        /*
        For each element at 'i' of the array starting from the second
            While the current position is positive and the element at the current position is smaller than it's predecessor
                Swap the element at current position with it's predecessor
                Substract 1 to current position
        End of algorithm
         */
        for (Integer i = 1; i < array.length; i++) {
            Integer current_pos = i;
            while ((current_pos > 0) && (array[current_pos].compareTo(array[current_pos - 1]) < 0)) {
                swapElements(array, current_pos, current_pos - 1);
                current_pos--;
            }
        }
    }
}
