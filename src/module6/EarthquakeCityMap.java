package module6;

import java.util.*;
import java.util.stream.Collectors;

import de.fhpotsdam.unfolding.UnfoldingMap;
import de.fhpotsdam.unfolding.data.Feature;
import de.fhpotsdam.unfolding.data.GeoJSONReader;
import de.fhpotsdam.unfolding.data.PointFeature;
import de.fhpotsdam.unfolding.geo.Location;
import de.fhpotsdam.unfolding.marker.AbstractShapeMarker;
import de.fhpotsdam.unfolding.marker.Marker;
import de.fhpotsdam.unfolding.marker.MultiMarker;
import de.fhpotsdam.unfolding.providers.Google;
import de.fhpotsdam.unfolding.providers.MBTilesMapProvider;
import de.fhpotsdam.unfolding.utils.MapUtils;
import parsing.ParseFeed;
import processing.core.PApplet;
import processing.core.PGraphics;

/**
 * EarthquakeCityMap
 * An application with an interactive map displaying earthquake data.
 * Author: UC San Diego Intermediate Software Development MOOC team
 *
 * @author Your name here
 *         Date: July 17, 2015
 */
public class EarthquakeCityMap extends PApplet {

    // We will use member variables, instead of local variables, to store the data
    // that the setUp and draw methods will need to access (as well as other methods)
    // You will use many of these variables, but the only one you should need to add
    // code to modify is countryQuakes, where you will store the number of earthquakes
    // per country.

    // You can ignore this.  It's to get rid of eclipse warnings
    private static final long serialVersionUID = 1L;

    // IF YOU ARE WORKING OFFILINE, change the value of this variable to true
    private static final boolean offline = false;

    /**
     * This is where to find the local tiles, for working without an Internet connection
     */
    public static String mbTilesString = "blankLight-1-3.mbtiles";


    //feed with magnitude 2.5+ Earthquakes
    private String earthquakesURL = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_week.atom";

    // The files containing city names and info and country names and info
    private String cityFile = "city-data.json";
    private String countryFile = "countries.geo.json";

    // The map
    private UnfoldingMap map;

    // Markers for each city
    private List<Marker> cityMarkers;

    // Markers for each earthquake
    private List<Marker> quakeMarkers;

    // A List of country markers
    private List<Marker> countryMarkers;

    // NEW IN MODULE 5
    private CommonMarker lastSelected;
    private CommonMarker lastClicked;

    public void setup() {
        // (1) Initializing canvas and map tiles
        size(900, 700, OPENGL);
        if (offline) {
            map = new UnfoldingMap(this, 200, 50, 650, 600, new MBTilesMapProvider(mbTilesString));
            earthquakesURL = "2.5_week.atom";  // The same feed, but saved August 7, 2015
        } else {
            map = new UnfoldingMap(this, 200, 50, 650, 600, new Google.GoogleMapProvider());
            // IF YOU WANT TO TEST WITH A LOCAL FILE, uncomment the next line
            //earthquakesURL = "2.5_week.atom";
        }
        MapUtils.createDefaultEventDispatcher(this, map);

        // FOR TESTING: Set earthquakesURL to be one of the testing files by uncommenting
        // one of the lines below.  This will work whether you are online or offline
        //earthquakesURL = "test1.atom";
        //earthquakesURL = "test2.atom";

        // Uncomment this line to take the quiz
        earthquakesURL = "quiz2.atom";


        // (2) Reading in earthquake data and geometric properties
        //     STEP 1: load country features and markers
        List<Feature> countries = GeoJSONReader.loadData(this, countryFile);
        countryMarkers = MapUtils.createSimpleMarkers(countries);

        //     STEP 2: read in city data
        List<Feature> cities = GeoJSONReader.loadData(this, cityFile);
        cityMarkers = new ArrayList<Marker>();
        for (Feature city : cities) {
            cityMarkers.add(new CityMarker(city));
        }

        //     STEP 3: read in earthquake RSS feed
        List<PointFeature> earthquakes = ParseFeed.parseEarthquake(this, earthquakesURL);
        quakeMarkers = new ArrayList<>();

        for (PointFeature feature : earthquakes) {
            //check if LandQuake
            if (isLand(feature)) {
                quakeMarkers.add(new LandQuakeMarker(feature));
            }
            // OceanQuakes
            else {
                quakeMarkers.add(new OceanQuakeMarker(feature));
            }
        }

        // could be used for debugging
        // printQuakes();

        // (3) Add markers to map
        //     NOTE: Country markers are not added to the map.  They are used
        //           for their geometric properties

        //sortAndPrint(5);
        //printQuakes();
        searchAndPrint();

        map.addMarkers(quakeMarkers);
        map.addMarkers(cityMarkers);


    }  // End setup


    public void draw() {
        background(0);
        map.draw();
        addKey();

    }

    private void printElementsOfList(ArrayList<Comparable> list, int num_items) {
        for (int i = 0; i < num_items; i++) {
            System.out.println(i + ") " + list.get(i));
        }
    }

    private void printElementsOfList(Comparable[] array, int num_items) {
        for (int i = 0; i < num_items; i++) {
            System.out.println(i + ") " + array[i]);
        }
    }

    private Comparable[] transformComparableListToArray(ArrayList<Comparable> guineaPig) {
        Comparable[] guineaPigArray = new Comparable[guineaPig.size()];

        for (int i = 0; i < guineaPigArray.length; i++) {
            guineaPigArray[i] = guineaPig.get(i);
        }
        return guineaPigArray;
    }

    private ArrayList<Comparable> transformListOfMarkersToComparables(List<Marker> transform) {
        return transform.stream().map(m -> (EarthquakeMarker) m).collect(Collectors.toCollection(ArrayList::new));
    }

    private void collectionsSortAndPrint(int numToPrint) {
        ArrayList<Comparable> guineaPig = transformListOfMarkersToComparables(quakeMarkers);

        Long start_time = null;
        Long end_time = null;

        start_time = System.currentTimeMillis();
        Collections.sort(guineaPig);
        end_time = System.currentTimeMillis();

        Long elapsed_time = end_time - start_time;
        System.out.println("======================//\\\\=====================");
        System.out.println("Ordering using Collections.sort() took: " + elapsed_time + " milis");

        int evaluated_num_to_print = guineaPig.size() < numToPrint ? guineaPig.size() : numToPrint;
        printElementsOfList(guineaPig, evaluated_num_to_print);
    }

    private void insertionSortAndPrint(int numToPrint) {
        ArrayList<Comparable> guineaPig = transformListOfMarkersToComparables(quakeMarkers);

        Long start_time = null;
        Long end_time = null;
        SorterAndSearcher mySorterAndSearcher = new SorterAndSearcher();
        Comparable[] guineaPigArray = transformComparableListToArray(guineaPig);

        start_time = System.currentTimeMillis();
        mySorterAndSearcher.insertionSort(guineaPigArray);
        end_time = System.currentTimeMillis();
        Long elapsed_time = end_time - start_time;
        System.out.println("======================//\\\\=====================");
        System.out.println("Ordering using SorterAndSearcher.insertionSort() took: " + elapsed_time + " milis");

        int evaluated_num_to_print = guineaPigArray.length < numToPrint ? guineaPigArray.length : numToPrint;
        printElementsOfList(guineaPigArray, evaluated_num_to_print);
    }

    private void selectionSortAndPrint(int numToPrint) {
        ArrayList<Comparable> guineaPig = transformListOfMarkersToComparables(quakeMarkers);

        Long start_time = null;
        Long end_time = null;
        SorterAndSearcher mySorterAndSearcher = new SorterAndSearcher();
        Comparable[] guineaPigArray = transformComparableListToArray(guineaPig);

        start_time = System.currentTimeMillis();
        mySorterAndSearcher.selectionSort(guineaPigArray);
        end_time = System.currentTimeMillis();
        Long elapsed_time = end_time - start_time;
        System.out.println("======================//\\\\=====================");
        System.out.println("Ordering using SorterAndSearcher.selectionSort() took: " + elapsed_time + " milis");

        int evaluated_num_to_print = guineaPigArray.length < numToPrint ? guineaPigArray.length : numToPrint;
        printElementsOfList(guineaPigArray, evaluated_num_to_print);
    }


    // and then call that method from setUp
    // TODO: Add the method:
    private void sortAndPrint(int numToPrint) {
        System.out.println(
                "=======================================================================================" +
                        "\nNext I will invoke three different methods that approaches sorting in different ways." +
                        "\nThe idea is to use my implementation of the sorting algorithms presented on this course" +
                        "\nand compare their performance against Collections, that's a Java class, by measuring" +
                        "\nthe completion times of each algorithm. After each sort some elements are printed to" +
                        "\nshow that despite performance differences the results are the same." +
                        "\n=======================================================================================");
        collectionsSortAndPrint(numToPrint);
        insertionSortAndPrint(numToPrint);
        selectionSortAndPrint(numToPrint);

    }

    private void linearSearchAndPrint(EarthquakeMarker em) {
        SorterAndSearcher mySorterAndSearcher = new SorterAndSearcher();
        Comparable[] guineaPig = transformComparableListToArray(transformListOfMarkersToComparables(quakeMarkers));

        Long start_time = null;
        Long end_time = null;

        start_time = System.currentTimeMillis();
        Comparable found = mySorterAndSearcher.linearSearch(guineaPig, em);
        end_time = System.currentTimeMillis();
        Long elapsed_time = end_time - start_time;
        System.out.println("======================//\\\\=====================");
        System.out.println("Search using SorterAndSearcher.linearSearch() took: " + elapsed_time + " milis");

        if (found != null) {
            System.out.println(found);
        } else {
            System.out.println("Not found!!!");
        }
    }

    private void binarySearchAndPrint(EarthquakeMarker em) {
        SorterAndSearcher mySorterAndSearcher = new SorterAndSearcher();
        Comparable[] guineaPig = transformComparableListToArray(transformListOfMarkersToComparables(quakeMarkers));
        System.out.println("======================//\\\\=====================");
        System.out.println("Sorting the array...");
        mySorterAndSearcher.insertionSort(guineaPig);


        Long start_time = null;
        Long end_time = null;

        start_time = System.currentTimeMillis();
        Comparable found = mySorterAndSearcher.binarySearch(guineaPig, em);
        end_time = System.currentTimeMillis();
        Long elapsed_time = end_time - start_time;

        System.out.println("Search using SorterAndSearcher.binarySearch() took: " + elapsed_time + " milis");

        if (found != null) {
            System.out.println(found);
        } else {
            System.out.println("Not found!!!");
        }
    }

    private void searchAndPrint() {
        System.out.println(
                "=======================================================================================" +
                        "\nAs I did with the sorting algorithms now I will make use of the search algorithms" +
                        "\nand will show how long takes each algorithm to find the same element on the list." +
                        "\nFor the binary search algorithm i will not take into account the time needed to sort the collection" +
                        "\n=======================================================================================");

        Random r = new Random();
        EarthquakeMarker em = (EarthquakeMarker) quakeMarkers.get(r.nextInt(quakeMarkers.size()));
        em.setProperty("title", "M 6.4 - 180km SE of Gizo, Solomon Islands");
        em.setProperty("depth", "5.0");
        em.setProperty("magnitude", "6.4");
        em.setProperty("radius", "12.8");
        em.setProperty("age", "Past Week");

        linearSearchAndPrint(em);
        binarySearchAndPrint(em);
    }

    /**
     * Event handler that gets called automatically when the
     * mouse moves.
     */
    @Override
    public void mouseMoved() {
        // clear the last selection
        if (lastSelected != null) {
            lastSelected.setSelected(false);
            lastSelected = null;

        }
        selectMarkerIfHover(quakeMarkers);
        selectMarkerIfHover(cityMarkers);
        //loop();
    }

    // If there is a marker selected
    private void selectMarkerIfHover(List<Marker> markers) {
        // Abort if there's already a marker selected
        if (lastSelected != null) {
            return;
        }

        for (Marker m : markers) {
            CommonMarker marker = (CommonMarker) m;
            if (marker.isInside(map, mouseX, mouseY)) {
                lastSelected = marker;
                marker.setSelected(true);
                return;
            }
        }
    }

    /**
     * The event handler for mouse clicks
     * It will display an earthquake and its threat circle of cities
     * Or if a city is clicked, it will display all the earthquakes
     * where the city is in the threat circle
     */
    @Override
    public void mouseClicked() {
        if (lastClicked != null) {
            unhideMarkers();
            lastClicked = null;
        } else if (lastClicked == null) {
            checkEarthquakesForClick();
            if (lastClicked == null) {
                checkCitiesForClick();
            }
        }
    }

    // Helper method that will check if a city marker was clicked on
    // and respond appropriately
    private void checkCitiesForClick() {
        if (lastClicked != null) return;
        // Loop over the earthquake markers to see if one of them is selected
        for (Marker marker : cityMarkers) {
            if (!marker.isHidden() && marker.isInside(map, mouseX, mouseY)) {
                lastClicked = (CommonMarker) marker;
                // Hide all the other earthquakes and hide
                for (Marker mhide : cityMarkers) {
                    if (mhide != lastClicked) {
                        mhide.setHidden(true);
                    }
                }
                for (Marker mhide : quakeMarkers) {
                    EarthquakeMarker quakeMarker = (EarthquakeMarker) mhide;
                    if (quakeMarker.getDistanceTo(marker.getLocation())
                            > quakeMarker.threatCircle()) {
                        quakeMarker.setHidden(true);
                    }
                }
                return;
            }
        }
    }

    // Helper method that will check if an earthquake marker was clicked on
    // and respond appropriately
    private void checkEarthquakesForClick() {
        if (lastClicked != null) return;
        // Loop over the earthquake markers to see if one of them is selected
        for (Marker m : quakeMarkers) {
            EarthquakeMarker marker = (EarthquakeMarker) m;
            if (!marker.isHidden() && marker.isInside(map, mouseX, mouseY)) {
                lastClicked = marker;
                // Hide all the other earthquakes and hide
                for (Marker mhide : quakeMarkers) {
                    if (mhide != lastClicked) {
                        mhide.setHidden(true);
                    }
                }
                for (Marker mhide : cityMarkers) {
                    if (mhide.getDistanceTo(marker.getLocation())
                            > marker.threatCircle()) {
                        mhide.setHidden(true);
                    }
                }
                return;
            }
        }
    }

    // loop over and unhide all markers
    private void unhideMarkers() {
        for (Marker marker : quakeMarkers) {
            marker.setHidden(false);
        }

        for (Marker marker : cityMarkers) {
            marker.setHidden(false);
        }
    }

    // helper method to draw key in GUI
    private void addKey() {
        // Remember you can use Processing's graphics methods here
        fill(255, 250, 240);

        int xbase = 25;
        int ybase = 50;

        rect(xbase, ybase, 150, 250);

        fill(0);
        textAlign(LEFT, CENTER);
        textSize(12);
        text("Earthquake Key", xbase + 25, ybase + 25);

        fill(150, 30, 30);
        int tri_xbase = xbase + 35;
        int tri_ybase = ybase + 50;
        triangle(tri_xbase, tri_ybase - CityMarker.TRI_SIZE, tri_xbase - CityMarker.TRI_SIZE,
                tri_ybase + CityMarker.TRI_SIZE, tri_xbase + CityMarker.TRI_SIZE,
                tri_ybase + CityMarker.TRI_SIZE);

        fill(0, 0, 0);
        textAlign(LEFT, CENTER);
        text("City Marker", tri_xbase + 15, tri_ybase);

        text("Land Quake", xbase + 50, ybase + 70);
        text("Ocean Quake", xbase + 50, ybase + 90);
        text("Size ~ Magnitude", xbase + 25, ybase + 110);

        fill(255, 255, 255);
        ellipse(xbase + 35,
                ybase + 70,
                10,
                10);
        rect(xbase + 35 - 5, ybase + 90 - 5, 10, 10);

        fill(color(255, 255, 0));
        ellipse(xbase + 35, ybase + 140, 12, 12);
        fill(color(0, 0, 255));
        ellipse(xbase + 35, ybase + 160, 12, 12);
        fill(color(255, 0, 0));
        ellipse(xbase + 35, ybase + 180, 12, 12);

        textAlign(LEFT, CENTER);
        fill(0, 0, 0);
        text("Shallow", xbase + 50, ybase + 140);
        text("Intermediate", xbase + 50, ybase + 160);
        text("Deep", xbase + 50, ybase + 180);

        text("Past hour", xbase + 50, ybase + 200);

        fill(255, 255, 255);
        int centerx = xbase + 35;
        int centery = ybase + 200;
        ellipse(centerx, centery, 12, 12);

        strokeWeight(2);
        line(centerx - 8, centery - 8, centerx + 8, centery + 8);
        line(centerx - 8, centery + 8, centerx + 8, centery - 8);


    }


    // Checks whether this quake occurred on land.  If it did, it sets the
    // "country" property of its PointFeature to the country where it occurred
    // and returns true.  Notice that the helper method isInCountry will
    // set this "country" property already.  Otherwise it returns false.
    private boolean isLand(PointFeature earthquake) {

        // IMPLEMENT THIS: loop over all countries to check if location is in any of them
        // If it is, add 1 to the entry in countryQuakes corresponding to this country.
        for (Marker country : countryMarkers) {
            if (isInCountry(earthquake, country)) {
                return true;
            }
        }

        // not inside any country
        return false;
    }

    // prints countries with number of earthquakes
    // You will want to loop through the country markers or country features
    // (either will work) and then for each country, loop through
    // the quakes to count how many occurred in that country.
    // Recall that the country markers have a "name" property,
    // And LandQuakeMarkers have a "country" property set.
    private void printQuakes() {
        // Referencia de Lambdas :)

        // https://docs.oracle.com/javase/tutorial/collections/streams/reduction.html
        // http://zeroturnaround.com/rebellabs/java-8-explained-applying-lambdas-to-java-collections/
        System.out.println(
                "=======================================================================================" +
                        "\nFor the printQuakes() method I've decided to delve into de the Stream API and Lambdas." +
                        "\nThe idea was to use the tools available in Java to work with collections to" +
                        "\nimprove performance. As some of you may have noticed nested loops are not very performant." +
                        "\n=======================================================================================");

        System.out.println("\n=========================");
        System.out.println("Earthquakes on the ocean:");
        System.out.println("=========================");
        Long mapOcean = quakeMarkers.stream()
                .filter(q -> !(q instanceof LandQuakeMarker))
                .collect(Collectors.counting());
        System.out.println("Total: " + mapOcean);

        System.out.println("\n=========================");
        System.out.println("Earthquakes by country:");
        System.out.println("=======================");
        Map<String, Long> mapEarth = quakeMarkers.stream()
                .filter(q -> q instanceof LandQuakeMarker)
                .collect(Collectors.groupingBy(q -> q.getStringProperty("country"),
                        Collectors.counting()));

        mapEarth.forEach((k, v) -> System.out.println(k + ": " + v));


    }


    // helper method to test whether a given earthquake is in a given country
    // This will also add the country property to the properties of the earthquake feature if
    // it's in one of the countries.
    // You should not have to modify this code
    private boolean isInCountry(PointFeature earthquake, Marker country) {
        // getting location of feature
        Location checkLoc = earthquake.getLocation();

        // some countries represented it as MultiMarker
        // looping over SimplePolygonMarkers which make them up to use isInsideByLoc
        if (country.getClass() == MultiMarker.class) {

            // looping over markers making up MultiMarker
            for (Marker marker : ((MultiMarker) country).getMarkers()) {

                // checking if inside
                if (((AbstractShapeMarker) marker).isInsideByLocation(checkLoc)) {
                    earthquake.addProperty("country", country.getProperty("name"));

                    // return if is inside one
                    return true;
                }
            }
        }

        // check if inside country represented by SimplePolygonMarker
        else if (((AbstractShapeMarker) country).isInsideByLocation(checkLoc)) {
            earthquake.addProperty("country", country.getProperty("name"));

            return true;
        }
        return false;
    }

}
