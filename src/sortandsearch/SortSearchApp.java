package sortandsearch;

import module6.SorterAndSearcher;

/**
 * Created by edwin on 28/10/16.
 */
public class SortSearchApp {

    public static void imprimirArray(Integer[] arreglo) {
        for (Integer i : arreglo) {
            System.out.print(" " + i);
        }
    }

    public static void main(String[] args) {
        Integer indiceTestigo = 1;
        SorterAndSearcher ss = new SorterAndSearcher();

        Integer[] arr = ss.generateIntegerArray(12, 250);
        System.out.println("Unordered Array");
        imprimirArray(arr);
        System.out.println("\nArray ordenado por inserción");
        ss.selectionSort(arr);
        imprimirArray(arr);

        System.out.println("\nBusqueda lineal del numero 88");
        System.out.println("Encontrado: " + ss.linearSearch(arr, 88));

        System.out.println("Busqueda lineal del numero " + arr[indiceTestigo]);
        System.out.println("Encontrado: " + ss.linearSearch(arr, arr[indiceTestigo]));

        System.out.println("Array ordenado por selección");
        ss.selectionSort(arr);
        imprimirArray(arr);


        Integer[] arr2 = ss.generateIntegerArray(12, 250);
        System.out.println("\n\n\nArray desordenado");
        imprimirArray(arr2);
        System.out.println("\nArray ordenado por inserción");
        ss.insertionSort(arr2);
        imprimirArray(arr2);

        System.out.println("\nBusqueda binaria del numero 88");
        System.out.println("Encontrado: " + ss.binarySearch(arr2, 88));

        System.out.println("Busqueda binaria del " + arr2[indiceTestigo]);
        System.out.println("Encontrado: " + ss.binarySearch(arr2, arr2[indiceTestigo]));
    }
}
